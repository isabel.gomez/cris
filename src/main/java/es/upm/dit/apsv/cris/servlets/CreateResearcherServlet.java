package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import es.upm.dit.apsv.cris.dao.ResearcherDAO;
import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.model.Researcher;


@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Researcher user = (Researcher) request.getSession().getAttribute("user");
		if(user.getId().equals("root")){
			String id = request.getParameter("id");
			String name = request.getParameter("name");
			String lastname = request.getParameter("lastname");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			ResearcherDAO dao = ResearcherDAOImplementation.getInstance();
			Researcher rold = dao.read(id);
			Researcher rnew = new Researcher();
			if(rold == null) {
				rnew.setId(id);
				rnew.setName(name);
				rnew.setLastname(lastname);
				rnew.setEmail(email);
				rnew.setPassword(password);
				dao.create(rnew);
				response.sendRedirect(request.getContextPath() + "/AdminServlet");
				}
		}
		else {
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
		}
	}
}
